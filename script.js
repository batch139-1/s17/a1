let students = [];

// item 3

function addStudent(student) {
  students.push(student.toLowerCase());
  console.log(`${student} was added to the student's list.`);
}

addStudent(`Joy`);
addStudent("John");
addStudent(`Allan`);
addStudent(`Chris`);

// item 4

function countStudents() {
  console.log(`There are a total of ${students.length} students enrolled`);
}

countStudents();

// item 5

function printStudent() {
  students.sort(),
    students.forEach(function (student) {
      console.log(student);
    });
}

// item 6

printStudent();

function findStudent(search) {
  const searched = students.filter((student) =>
    student.includes(search.toLowerCase())
  );
  if (searched.length > 1) {
    console.log(`${searched.join(`, `)} are enrollees`);
  }
  if (searched.length == 1) {
    console.log(`${searched[0]} is an enrolee`);
  } else if (searched.length == 0) {
    console.log(`${search.toLowerCase()} is not an enrolee`);
  }
}

findStudent(`Allan`);
findStudent(`Henry`);
findStudent(`j`);
